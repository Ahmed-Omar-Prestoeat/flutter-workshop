import 'package:flutter/material.dart';
import 'package:widget_tests/memories_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Widget Tests',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: const MemoriesPage(),
    );
  }
}
