class DummyData {
  const DummyData({
    required this.imageUrl,
    required this.title,
    required this.date,
  });
  final String imageUrl;
  final String title;
  final String date;
}

const List<DummyData> memories = [
  DummyData(
    title: "صاحبتي",
    date: '2023-01-01',
    imageUrl: "assets/images/image_1.jpeg",
  ),
  DummyData(
    title: "شامخة",
    date: '2023-01-01',
    imageUrl: "assets/images/image_2.jpeg",
  ),
  DummyData(
    title: "عزوزتي",
    date: '2023-01-01',
    imageUrl: "assets/images/image_3.jpeg",
  ),
  DummyData(
    title: "ملك ال Engine",
    date: '2023-01-01',
    imageUrl: "assets/images/image_4.jpeg",
  ),
  DummyData(
    title: "️❤️‍🔥عصومي❤️‍🔥",
    date: '2023-01-01',
    imageUrl: "assets/images/image_5.jpeg",
  ),
  DummyData(
    title: "Hacker",
    date: '2023-01-01',
    imageUrl: "assets/images/image_6.jpeg",
  ),
];
