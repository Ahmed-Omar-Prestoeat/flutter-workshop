import 'package:flutter/material.dart';

class ImageCardWidget extends StatelessWidget {
  const ImageCardWidget({
    super.key,
    required this.title,
    required this.date,
    required this.imageUrl,
  });

  final String title;
  final String date;
  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 24,
        horizontal: 16,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(child: Image.asset(imageUrl)),
          const SizedBox(height: 12),
          Text(
            key: const Key("title"),
            title,
            style: const TextStyle(fontSize: 32),
          ),
          const SizedBox(height: 8),
          Text(
            key: const Key("date"),
            date,
            style: const TextStyle(
              fontSize: 32,
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}
