import 'package:flutter/material.dart';

import 'dummy_data.dart';
import 'image_card_widget.dart';

class MemoriesPage extends StatefulWidget {
  const MemoriesPage({super.key});

  @override
  State<MemoriesPage> createState() => _MemoriesPageState();
}

class _MemoriesPageState extends State<MemoriesPage> {
  late final PageController pageController;

  bool isShowingFloatingActionButton = false;

  @override
  void initState() {
    pageController = PageController();
    pageController.addListener(() {
      print(pageController.offset);
      final page = pageController.page;
      if (page == null) return;
      if (page > 1) {
        setState(() {
          isShowingFloatingActionButton = true;
        });
      } else {
        setState(() {
          isShowingFloatingActionButton = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: isShowingFloatingActionButton
          ? FloatingActionButton(
              child: const Text("GO UP"),
              onPressed: () {
                pageController.animateToPage(
                  0,
                  duration: const Duration(seconds: 1),
                  curve: Curves.elasticInOut,
                );
              },
            )
          : null,
      appBar: AppBar(title: const Text("Memories")),
      body: PageView.builder(
        controller: pageController,
        itemCount: memories.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          final memorie = memories[index];
          return ImageCardWidget(
            title: memorie.title,
            date: memorie.date,
            imageUrl: memorie.imageUrl,
          );
        },
      ),
    );
  }
}
